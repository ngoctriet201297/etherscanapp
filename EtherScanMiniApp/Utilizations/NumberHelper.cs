namespace EtherScanMiniApp.Utilizations
{
    public static class NumberHelper
    {
        public static int HexToInt(this string hex)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(hex)) return 0;
                return Convert.ToInt32(hex, 16);
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        public static decimal HexToDec(this string hex)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(hex)) return 0;
                return Convert.ToInt64(hex, 16);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static string NumberToHex(this int num)
        {
            return num.ToString("X");
        }
    }
}