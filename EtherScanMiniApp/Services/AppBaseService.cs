namespace EtherScanMiniApp.Services
{
    public class AppBaseService
    {
        public async Task<TData> HandlingWithEtryAndException<TData>(Func<Task<TData>> handler, Action<Exception> exceptionHandler, int numberOfRetries = 3) where TData : class
        {
            var currentNumberOfRetries = 0;
            Random rnd = new Random();
            int delayTime = rnd.Next(1,3)*1000;
            while (currentNumberOfRetries < numberOfRetries)
            {
                try
                {
                    currentNumberOfRetries++;
                    return await handler();
                }
                catch (Exception e)
                {
                    if (currentNumberOfRetries >= numberOfRetries)
                    {
                        exceptionHandler(e);
                    }
                    await Task.Delay(delayTime);
                }
            }
            return null;
        }
    }
}