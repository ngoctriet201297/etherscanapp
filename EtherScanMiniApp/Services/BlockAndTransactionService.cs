using System.Diagnostics;
using EtherScanMiniApp.Clients;
using EtherScanMiniApp.Database;
using EtherScanMiniApp.Database.Entities;
using EtherScanMiniApp.Database.Repositories;
using EtherScanMiniApp.Dtos.EtherScan;
using Serilog;
using EtherScanMiniApp.Settings;
using EtherScanMiniApp.Utilizations;
using Microsoft.Extensions.Logging;

namespace EtherScanMiniApp.Services
{
    public class BlockAndTransactionService : AppBaseService, IBlockAndTransactionService
    {
        private IEtherScanClient _etherScanClient;
        private IRepository<Block> _blockRepository;
        private IRepository<Transaction> _transactionRepository;
        private readonly ILogger<BlockAndTransactionService> _logger;

        public BlockAndTransactionService(IEtherScanClient etherScanClient,
                                          IRepository<Block> blockRepository,
                                          IRepository<Transaction> transactionRepository,
                                          ILogger<BlockAndTransactionService> logger)
        {
            _etherScanClient = etherScanClient;
            _blockRepository = blockRepository;
            _transactionRepository = transactionRepository;
            _logger = logger;
        }

        public async Task IndexBlocksAndTransactionRange(List<int> listOfBlockNumbers)
        {
            foreach (var blockNumber in listOfBlockNumbers)
            {
                var hexValue = blockNumber.NumberToHex();
                await this.IndexBlocksAndTransaction(hexValue);
            }
        }

        #region Private
        private async Task IndexBlocksAndTransaction(string tag)
        {
            _logger.LogInformation($"Start to index block and transaction with tag {tag}");
            var timer = new Stopwatch();
            timer.Start();
            var block = await _etherScanClient.GetBlockByNumber(tag);
            if (block == null)
            {
                timer.Stop();
                _logger.LogError($"Block[{tag}] doesn't exist, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
                return;
            }

            var blockEntity = await HandlingWithEtryAndException(async () =>
            {
                return await _blockRepository.Insert(new Block
                {
                    BlockNumber = tag.HexToInt(),
                    Hash = block.Hash,
                    ParentHash = block.ParentHash,
                    Miner = block.Miner,
                    GasLimit = block.GasLimit.HexToDec(),
                    GasUsed = block.GasUsed.HexToDec()
                });
            }, (ex) =>
            {
                _logger.LogError(ex, $"something wrong with inserting block {tag}");
            });

            if (blockEntity == null)
            {
                timer.Stop();
                _logger.LogError($"Block[{tag}] insert failed, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
                return;
            }

            var numberHex = await _etherScanClient.GetBlockTransactionCountByNumber(tag);
            int decValue = numberHex?.HexToInt() ?? 0;
            if (decValue == 0)
            {
                timer.Stop();
                _logger.LogError($"Block[{tag}] doesn't contain transaction information, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
                return;
            }

            var transactions = new List<GetTransactionByBlockNumberAndIndexOutputDto>(decValue);
            for (int i = 0; i < decValue; i++)
            {
                var transaction = await _etherScanClient.GetTransactionByBlockNumberAndIndex(tag, String.Format("0X{0:X}", i));
                if (transaction != null)
                {
                    transactions.Add(transaction);
                }
            }
            if (transactions.Any())
            {
                var isError = false;
                await HandlingWithEtryAndException(async () =>
                {
                    await _transactionRepository.InsertRange(transactions.Select(t => new Transaction
                    {
                        BlockID = blockEntity.BlockID,
                        Hash = t.Hash,
                        From = t.From,
                        To = t.To,
                        Value = t.Value.HexToDec(),
                        Gas = t.Gas.HexToDec(),
                        GasPrice = t.GasPrice.HexToDec(),
                        TransactionIndex = t.TransactionIndex.HexToInt()
                    }).ToList());
                    return Task.CompletedTask;
                }, (ex) =>
                {
                    timer.Stop();
                    _logger.LogError($"Transaction[{tag}] insert failed, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
                    isError = true;
                });
                if (isError) return;
            }

            timer.Stop();
            _logger.LogInformation($"Finished to index block and transaction with tag {tag}, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
            TimeSpan timeTaken = timer.Elapsed;
        }
        #endregion
    }
}