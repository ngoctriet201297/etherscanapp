namespace EtherScanMiniApp.Services
{
    public interface IBlockAndTransactionService
    {
        Task IndexBlocksAndTransactionRange(List<int> listOfBlockNumbers);
    }
}