using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EtherScanMiniApp.Database.Entities
{
    [Table("transactions")]
    public class Transaction
    {
        [Column("transactionID")]
        [Key]
        public int TransactionID { get; set; }
        [Column("blockID")]
        public int BlockID { get; set; }
        public Block Block { get; set; }
        [Column("hash")]
        [MaxLength(66)]
        public string? Hash { get; set; }
        [Column("from")]
        [MaxLength(42)]
        public string? From { get; set; }
        [Column("to")]
        [MaxLength(42)]
        public string? To { get; set; }
        [Column("value")]
        public decimal Value { get; set; }
        [Column("gas")]
        public decimal Gas { get; set; }
        [Column("gasPrice")]
        public decimal GasPrice { get; set; }
        [Column("transactionIndex")]
        public int TransactionIndex { get; set; }
    }
}