using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EtherScanMiniApp.Database.Entities
{
    [Table("blocks")]
    public class Block
    {
        [Column("blockID")]
        [Key]
        public int BlockID { get; set; }
        [Column("blockNumber")]
        public int BlockNumber { get; set; }
        [MaxLength(66)]
        [Column("hash")]
        public string? Hash { get; set; }
        [MaxLength(66)]
        [Column("parentHash")]
        public string? ParentHash { get; set; }
        [MaxLength(42)]
        [Column("miner")]
        public string? Miner { get; set; }
        [Column("blockReward")]
        public decimal BlockReward { get; set; }
        [Column("gasLimit")]
        public decimal GasLimit { get; set; }
        [Column("gasUsed")]
        public decimal GasUsed { get; set; }
    }
}