namespace EtherScanMiniApp.Database.Repositories
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> Insert(TEntity data);
        Task InsertRange(List<TEntity> data);
    }
}