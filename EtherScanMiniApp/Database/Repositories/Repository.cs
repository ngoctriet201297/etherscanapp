namespace EtherScanMiniApp.Database.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly EtherScanDbContext _dbContext;

        public Repository(EtherScanDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TEntity> Insert(TEntity data)
        {
            await _dbContext.Set<TEntity>().AddAsync(data);
            await _dbContext.SaveChangeWithRetry();
            return data;
        }

        public async Task InsertRange(List<TEntity> data)
        {
            await _dbContext.Set<TEntity>().AddRangeAsync(data);
            await _dbContext.SaveChangeWithRetry();
        }
    }
}