CREATE DATABASE `etherscan`;
USE `etherscan`;
CREATE TABLE `blocks` (
  `blockID` int NOT NULL AUTO_INCREMENT,
  `blockNumber` int NOT NULL,
  `hash` varchar(66) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `parentHash` varchar(66) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `miner` varchar(42) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `blockReward` decimal(65,30) NOT NULL,
  `gasLimit` decimal(65,30) NOT NULL,
  `gasUsed` decimal(65,30) NOT NULL,
  PRIMARY KEY (`blockID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `transactions` (
  `transactionID` int NOT NULL AUTO_INCREMENT,
  `blockID` int NOT NULL,
  `hash` varchar(66) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `from` varchar(42) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `to` varchar(42) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `value` decimal(65,30) NOT NULL,
  `gas` decimal(65,30) NOT NULL,
  `gasPrice` decimal(65,30) NOT NULL,
  `transactionIndex` int NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `IX_transactions_blockID` (`blockID`),
  CONSTRAINT `FK_transactions_blocks_blockID` FOREIGN KEY (`blockID`) REFERENCES `blocks` (`blockID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1417 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
