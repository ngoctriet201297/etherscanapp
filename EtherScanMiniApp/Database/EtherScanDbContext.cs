using EtherScanMiniApp.Database.Entities;
using EtherScanMiniApp.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace EtherScanMiniApp.Database
{
    public class EtherScanDbContext : DbContext
    {
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        private string ConnectionString { get; }

        public EtherScanDbContext(DbContextOptions<EtherScanDbContext> dbContextOptions, IOptions<AppSettingOption> appSettingOption) : base(dbContextOptions)
        {
            ConnectionString = appSettingOption.Value.ConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(ConnectionString, ServerVersion.AutoDetect(ConnectionString));
        }

        public async Task SaveChangeWithRetry(int numberOfRetries = 2)
        {
            var currentNumberOfRetries = 0;
            while (currentNumberOfRetries < numberOfRetries)
            {
                try
                {
                    currentNumberOfRetries++;
                    await this.SaveChangesAsync();
                    break;
                }
                catch (Exception e)
                {
                    if (currentNumberOfRetries >= numberOfRetries)
                    {
                        throw;
                    }
                    await Task.Delay(2000);
                }
            }
        }
    }
}