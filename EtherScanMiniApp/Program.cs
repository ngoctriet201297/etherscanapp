﻿using EtherScanMiniApp.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using EtherScanMiniApp.Settings;
using Serilog;
using EtherScanMiniApp.Database.Repositories;
using EtherScanMiniApp.Services;
using EtherScanMiniApp.HostedServices;
using EtherScanMiniApp.Clients;

var HostBuilder = new HostBuilder();
await HostBuilder
        .ConfigureAppConfiguration(configurationBuilder =>
        {
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json", optional: false);
        })
        .ConfigureServices((hostContext, services) =>
        {
            var configuration = hostContext.Configuration;
            var appsettings = configuration.GetSection("AppSettings").Get<AppSettingOption>();

            services.AddSingleton<ILogger>((serviceProvider) =>
            {
                Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss dd-MMM-yyyy} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                        .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day, outputTemplate: "[{Timestamp:HH:mm:ss dd-MMM-yyyy} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                        .CreateLogger();
                return Log.Logger;
            });
            services.AddDbContext<EtherScanDbContext>(options =>
                options.UseMySql(configuration.GetConnectionString(appsettings.ConnectionString), ServerVersion.AutoDetect(appsettings.ConnectionString)),
                ServiceLifetime.Scoped
                );
            services.Configure<AppSettingOption>(configuration.GetSection("AppSettings"));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IBlockAndTransactionService, BlockAndTransactionService>();
            services.AddTransient<IEtherScanClient, EtherScanClient>();
            services.AddHostedService<BlockTransactionCollectorHostedService>();
        })
        .ConfigureLogging(configureLogging =>
        {
            configureLogging.AddSerilog(
                configureLogging.Services.BuildServiceProvider().GetRequiredService<ILogger>(),
                dispose: true);
        })
        .RunConsoleAsync();