﻿namespace EtherScanMiniApp.Dtos.EtherScan
{
    public class GetTransactionByBlockNumberAndIndexOutputDto
    {
        public string From { get; set; }
        public string Gas { get; set; }
        public string GasPrice { get; set; }
        public string Hash { get; set; }
        public string To { get; set; }
        public string TransactionIndex { get; set; }
        public string Value { get; set; }
    }
}
