﻿namespace EtherScanMiniApp.Dtos
{
    public class BaseResponse<TData>
    {
        public string Jsonrpc { get; set; }
        public int Id { get; set; }
        public TData Result { get; set; }
    }
}
