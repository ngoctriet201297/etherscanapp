namespace EtherScanMiniApp.Settings
{
    public class AppSettingOption
    {
        public string ConnectionString { get; set; }
        public string EtherScanApi { get; set; }
        public int NumberOfTaskToGetAndInsertBlockAndTransaction { get; set; }
        public int FromBlockNumber { get; set; }
        public int ToBlockNumber { get; set; }
        public string EtherScanApiKey { get; set; }
        public string GetTransactionByBlockNumberAndIndexPath { get; set; }
        public string GetBlockByNumberPath { get; set; }
        public string GetBlockTransactionCountByNumberPath { get; set; }
    }
}