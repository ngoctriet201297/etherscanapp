﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EtherScanMiniApp.Migrations
{
    /// <inheritdoc />
    public partial class AllowColumnCanBeNull : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "to",
                table: "transactions",
                type: "varchar(42)",
                maxLength: 42,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "hash",
                table: "transactions",
                type: "varchar(66)",
                maxLength: 66,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "from",
                table: "transactions",
                type: "varchar(42)",
                maxLength: 42,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "parentHash",
                table: "blocks",
                type: "varchar(66)",
                maxLength: 66,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "miner",
                table: "blocks",
                type: "varchar(42)",
                maxLength: 42,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "hash",
                table: "blocks",
                type: "varchar(66)",
                maxLength: 66,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "transactions",
                keyColumn: "to",
                keyValue: null,
                column: "to",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "to",
                table: "transactions",
                type: "varchar(42)",
                maxLength: 42,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "transactions",
                keyColumn: "hash",
                keyValue: null,
                column: "hash",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "hash",
                table: "transactions",
                type: "varchar(66)",
                maxLength: 66,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "transactions",
                keyColumn: "from",
                keyValue: null,
                column: "from",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "from",
                table: "transactions",
                type: "varchar(42)",
                maxLength: 42,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "blocks",
                keyColumn: "parentHash",
                keyValue: null,
                column: "parentHash",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "parentHash",
                table: "blocks",
                type: "varchar(66)",
                maxLength: 66,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "blocks",
                keyColumn: "miner",
                keyValue: null,
                column: "miner",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "miner",
                table: "blocks",
                type: "varchar(42)",
                maxLength: 42,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(42)",
                oldMaxLength: 42,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "blocks",
                keyColumn: "hash",
                keyValue: null,
                column: "hash",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "hash",
                table: "blocks",
                type: "varchar(66)",
                maxLength: 66,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(66)",
                oldMaxLength: 66,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }
    }
}
