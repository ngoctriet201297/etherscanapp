using System.Diagnostics;
using EtherScanMiniApp.Services;
using EtherScanMiniApp.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace EtherScanMiniApp.HostedServices;

public class BlockTransactionCollectorHostedService : BackgroundService
{
    private readonly ILogger<BlockTransactionCollectorHostedService> _logger;
    private IServiceProvider Services { get; }
    private AppSettingOption AppSettings { get; }

    public BlockTransactionCollectorHostedService(ILogger<BlockTransactionCollectorHostedService> logger,
                                                  IServiceProvider services,
                                                  IOptions<AppSettingOption> appSettingOption)
    {
        _logger = logger;
        Services = services;
        AppSettings = appSettingOption.Value;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await DoWork(stoppingToken);
    }

    private async Task DoWork(CancellationToken stoppingToken)
    {
        var timer = new Stopwatch();
        timer.Start();
        _logger.LogInformation($"Start Handling block number from {AppSettings.FromBlockNumber} to {AppSettings.ToBlockNumber} with {AppSettings.NumberOfTaskToGetAndInsertBlockAndTransaction} asynchronous Tasks");
        var partitionsOfBlockNumbers = ToPartitionBlockNumbers(AppSettings.FromBlockNumber, AppSettings.ToBlockNumber, AppSettings.NumberOfTaskToGetAndInsertBlockAndTransaction);

        var tasks = new List<Task>();
        foreach (var listOfBlockNumbers in partitionsOfBlockNumbers.Values.ToList())
        {
            var task = Task.Run(async () =>
               {
                   using (var scope = Services.CreateScope())
                   {
                       var blockAndTransactionService = scope.ServiceProvider.GetRequiredService<IBlockAndTransactionService>();
                        await blockAndTransactionService.IndexBlocksAndTransactionRange(listOfBlockNumbers);
                   }
               });
            tasks.Add(task);
        }
        await Task.WhenAll(tasks);
        timer.Stop();
        _logger.LogInformation($"Finish Handling block number from {AppSettings.FromBlockNumber} to {AppSettings.ToBlockNumber}, processing time: {timer.Elapsed.ToString(@"m\:ss\.fff")}");
    }

    private Dictionary<int, List<int>> ToPartitionBlockNumbers(int fromBlockNumber, int toBlockNumber, int numberOfPartitions)
    {
        var partitions = new Dictionary<int, List<int>>();
        for (int blockNumber = fromBlockNumber; blockNumber <= toBlockNumber; blockNumber++)
        {
            var index = blockNumber % numberOfPartitions;
            if (!partitions.ContainsKey(index))
            {
                partitions.Add(index, new List<int>());
            }
            var list = partitions[index];
            list.Add(blockNumber);
            partitions[index] = list;
        }
        return partitions;
    }
}