﻿using EtherScanMiniApp.Dtos.EtherScan;

namespace EtherScanMiniApp.Clients
{
    public interface IEtherScanClient
    {
        Task<GetBlockByNumberOutputDto?> GetBlockByNumber(string tag);
        Task<string?> GetBlockTransactionCountByNumber(string tag);
        Task<GetTransactionByBlockNumberAndIndexOutputDto?> GetTransactionByBlockNumberAndIndex(string tag, string index);
    }
}
