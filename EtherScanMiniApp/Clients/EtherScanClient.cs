﻿using EtherScanMiniApp.Database;
using EtherScanMiniApp.Database.Entities;
using EtherScanMiniApp.Database.Repositories;
using EtherScanMiniApp.Dtos;
using EtherScanMiniApp.Dtos.EtherScan;
using EtherScanMiniApp.Utilizations;
using Newtonsoft.Json;
using Serilog;
using System.Collections.Specialized;
using System.Web;
using EtherScanMiniApp.Settings;
using EtherScanMiniApp.Services;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace EtherScanMiniApp.Clients
{
    public class EtherScanClient : AppBaseService, IEtherScanClient
    {
        private HttpClient client = new HttpClient();
        private UriBuilder builder;
        private AppSettingOption AppSettings;
        private readonly ILogger<EtherScanClient> _logger;

        public EtherScanClient(IOptions<AppSettingOption> appSettingOption,
                               ILogger<EtherScanClient> logger)
        {
            AppSettings = appSettingOption.Value;
            builder = new UriBuilder(AppSettings.EtherScanApi);
            _logger = logger;
        }

        public async Task<GetBlockByNumberOutputDto?> GetBlockByNumber(string tag)
        {
            var result = await HandlingWithEtryAndException(async () =>
            {
                builder.Port = -1;
                var query = CreateQueryParam(new Dictionary<string, string> {
                 {"action", AppSettings.GetBlockByNumberPath},
                 {"tag", tag},
                 {"boolean", "true"}
                 });
                builder.Query = query.ToString();
                string url = builder.ToString();
                var stringResp = await client.GetStringAsync(url);
                var resp = JsonConvert.DeserializeObject<BaseResponse<GetBlockByNumberOutputDto>>(stringResp);

                return resp?.Result;
            }, (Exception e) =>
            {
                _logger.LogError(e, $"something wrong with GetBlockByNumber {tag}");
            });

            return result;
        }

        public async Task<string?> GetBlockTransactionCountByNumber(string tag)
        {
            return await HandlingWithEtryAndException(async () =>
            {
                builder.Port = -1;
                var query = CreateQueryParam(new Dictionary<string, string> {
                 {"action", AppSettings.GetBlockTransactionCountByNumberPath},
                 {"tag", tag},
                 });
                builder.Query = query.ToString();
                string url = builder.ToString();
                var stringResp = await client.GetStringAsync(url);
                var resp = JsonConvert.DeserializeObject<BaseResponse<string>>(stringResp);

                return resp?.Result;
            }, (ex) =>
            {
                _logger.LogError(ex, $"something wrong with GetBlockTransactionCountByNumber {tag}");
            });
        }

        public async Task<GetTransactionByBlockNumberAndIndexOutputDto?> GetTransactionByBlockNumberAndIndex(string tag, string index)
        {
            return await HandlingWithEtryAndException(async () =>
            {
                builder.Port = -1;
                var query = CreateQueryParam(new Dictionary<string, string> {
                 {"action", AppSettings.GetTransactionByBlockNumberAndIndexPath},
                 {"tag", tag},
                 {"index", index}
                 });
                builder.Query = query.ToString();
                string url = builder.ToString();
                var stringResp = await client.GetStringAsync(url);
                BaseResponse<GetTransactionByBlockNumberAndIndexOutputDto> resp;
                resp = JsonConvert.DeserializeObject<BaseResponse<GetTransactionByBlockNumberAndIndexOutputDto>>(stringResp);
                return resp?.Result;
            }, (ex) =>
            {
                _logger.LogError(ex, $"something wrong with GetTransactionByBlockNumberAndIndex {tag}");
            });
        }


        #region Private 
        private NameValueCollection CreateQueryParam(Dictionary<string, string> queryParams)
        {
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["module"] = "proxy";
            query["apikey"] = AppSettings.EtherScanApiKey;
            foreach (KeyValuePair<string, string> entry in queryParams)
            {
                query[entry.Key] = entry.Value;
            }
            return query;
        }
        #endregion
    }
}
