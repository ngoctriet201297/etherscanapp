# How to run console app
## Install Application
1. MySQL
2. Net 7
3. dotnet-ef

## Update Configuration
1. Go to ``EtherScanMiniApp/appsettings.json`` to update configuration (ConnectionString,...)
    ``` 
    "AppSettings": {
        // Connection string
        "ConnectionString": "Server=localhost;Database=etherscan;Uid=root;Pwd=123456789;",

        // URL to get block and transaction
        "EtherScanApi": "https://api.etherscan.io/api",

        // It's similar as number of thread to handle get and insert block/transaction
        "NumberOfTaskToGetAndInsertBlockAndTransaction": 5, 

        // from block number
        "FromBlockNumber": 12100001, 

        // to block number
        "ToBlockNumber": 12100500 ,

        "EtherScanApiKey": "KQF375DZN2ERXRRXBNMFG2CXMT3EVADYS5",
        "GetTransactionByBlockNumberAndIndexPath": "eth_getTransactionByBlockNumberAndIndex",
        "GetBlockByNumberPath": "eth_getBlockByNumber",
        "GetBlockTransactionCountByNumberPath": "eth_getBlockTransactionCountByNumber"
    }
    ```
## Create Database
1. Go to ``EtherScanMiniApp`` and run ``dotnet ef database update``

## Now you can run application 
1. Go to ``EtherScanMiniApp`` and run ``dotnet run``